
import sheridan.DiscountByAmount;
import sheridan.DiscountByPercentage;

public class ShoppingFactory {
    private static ShoppingCartFactory factory;
    
    //private default constructor --> so that  no one can instantiate from outside
    private ShoppingFactory()
    {}
    
    public static ShoppingCartFactory getInstance()
    {
        if(factory==null)
            factory = new ShoppingFactory();
        return factory;
    }
   `
    public DiscountByAmount getDiscount( DiscountTypes type) {
        switch ( type ) {
            case DISCOUNTBYAMOUNT : return new DiscountByAmount();
            case DISCOUNTBYPERCENTAGE : return new DiscountByPercentage();
        }
        return null;
    }
    }
        
